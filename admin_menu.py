import sys
import os
import cx_Oracle as cx
con=cx.connect('bank/vishal@127.0.0.1/xe')
cur=con.cursor()
def admin_menu():
    os.system('cls')
    validate=False
    prompt = int(raw_input("1. Print Closed Accounts History\n\n2. FD Report of a Customer"
                           "\n\n3. FD Report of a Customer vis-a-vis another Customer"
                           "\n\n4. FD Report w.r.t a particular FD amount \n\n5. Loan Report of a Customer"
                           "\n\n6. Loan Report of a Customer vis-a-vis another Customer"
                           "\n\n7. Loan Report w.r.t a particular Loan amount\n"
                           "\n8. Loan - FD Report of Customers\n\n9. Report of Customers who are yet to avail a loan\n"
                           "\n10. Report of Customers who are yet to open a FD account"
                           "\n\n11. Report of Customers who neither have a loan nor an FD account with the bank"
                           "\n\n0. Admin Logout\n"))
    ###################################################################################################################

    if prompt==1:
        os.system('cls')
        cur.execute("SELECT account_num,date_closed FROM cust_account where account_status='closed'")
        x=cur.fetchall()
        print "\n***********************************************************\n"
        print " ___________________________________"
        print "| Account Number |    Date Closed   |"
        print "|                |    (yy-mm-dd)    |"
        print "|________________|__________________|"
        for i in x:
            date = str(i[1])
            print "| ", repr(i[0]).rjust(12), " | ", repr(date[2:11]).rjust(14), " | "
        print "|________________|__________________|"

        print "\n***********************************************************\n"
        raw_input("\nPress Enter To GO BACK!\n")
        admin_menu()
    ###################################################################################################################

    elif prompt == 2:
        os.system('cls')
        while validate <>True:
            try:
                cust_id=int(raw_input("Enter Customer ID: "))
            except:
                print "Invalid ID.Please RETRY!"
                raw_input("\nPress Enter To GO BACK!\n")
                admin_menu()
            cur.execute("SELECT account_num FROM cust_account where cust_id=:1 and account_type='fd'",{'1':cust_id})
            x=cur.fetchall()
            if x is None:
                print "N/A"
            else:
                print "\n***********************************************************\n"
                print " ________________________________________________"
                print "| Account Number |     Amount       |    Term    |"
                print "|________________|__________________|____________|"
                for i in x:
                    cur.execute("SELECT amount,term FROM cust_fd where account_num=:1",{'1':i[0]})
                    y=cur.fetchone()
                    print "| ", repr(i[0]).rjust(12), " | ", repr(y[0]).rjust(14), " | ", repr(y[1]).rjust(8), " | "
                print "|________________|__________________|____________|"

                print "\n***********************************************************\n"
                validate=True
        raw_input("\nPress Enter To GO BACK!\n")
        admin_menu()

    ###################################################################################################################

    elif prompt==3:
        os.system('cls')
        while validate <>True:
            sum=0
            try:
                cust_id=int(raw_input("Enter Customer ID: "))
            except:
                print "Invalid ID.Please RETRY!"
                raw_input("\nPress Enter To GO BACK!\n")
                admin_menu()
            cur.execute("SELECT account_num FROM cust_account where cust_id=:1 and account_type='fd'",{'1':cust_id})
            x=cur.fetchall()
            for i in x:
                cur.execute("SELECT amount,term FROM cust_fd where account_num=:1", {'1': i[0]})
                sum+= cur.fetchone()[0]
            #print sum
            if x is None:
                print "N/A"
            else:
                cur.execute(
                    "select  cust_account.cust_id,cust_account.account_num,cust_fd.amount,cust_fd.term FROM cust_fd inner join "
                    "cust_account ON cust_fd.account_num=cust_account.account_num where cust_fd.amount >:1 and "
                    "cust_account.account_type='fd' ", {'1': sum})
                x = cur.fetchall()
                print "\n***********************************************************\n"
                print " _______________________________________________________________"
                print "| Customer ID    |     A/c Number   |    Amount    |    Term    |"
                print "|________________|__________________|______________|____________|"
                for i in x:
                    print "| ", repr(i[0]).rjust(12), " | ", repr(i[1]).rjust(14), " | ", repr(i[2]).rjust(10), " | ",repr(i[3]).rjust(8)
                print "|________________|__________________|______________|____________|"

                print "\n***********************************************************\n"
                validate=True
        raw_input("\nPress Enter To GO BACK!\n")
        admin_menu()

    ###################################################################################################################

    elif prompt==4:
        os.system('cls')
        while validate <>True:
            amount=float(raw_input("Enter Amount: "))
            if amount <=0 or amount%1000 <> 0:
                print "Enter valid Amount."
            else:
                validate=True
        cur.execute("select  cust_account.cust_id,cust_fd.amount FROM cust_fd inner join "
                    "cust_account ON cust_fd.account_num=cust_account.account_num where cust_fd.amount >:1 and "
                    "cust_account.account_type='fd' ",{'1':amount})
        x=cur.fetchall()
        print "\n***********************************************************\n"
        print " _____________________________________________________________________"
        print "| Customer ID    |     First Name   |     Last Name    |     Amount   |"
        print "|________________|__________________|__________________|______________|"
        for i in x:
            cur.execute("SELECT first_name,last_name FROM cust_info where cust_id=:1",{'1':i[0]})
            y=cur.fetchone()
            print "| ", repr(i[0]).rjust(12), " | ", repr(y[0]).rjust(14), " | ", repr(y[1]).rjust(14), " | ", repr(i[1]).rjust(10), " | "
        print "|________________|__________________|__________________|______________|"

        print "\n***********************************************************\n"
        raw_input("\nPress Enter To GO BACK!\n")
        admin_menu()
    ###################################################################################################################

    elif prompt==5:
        os.system('cls')
        while validate <>True:
            try:
                cust_id=int(raw_input("Enter Customer ID: "))
            except:
                print "Invalid ID.Please RETRY!"
                raw_input("\nPress Enter To GO BACK!\n")
                admin_menu()
            cur.execute("SELECT account_num FROM cust_account where cust_id=:1 and account_type='loan'",{'1':cust_id})
            x=cur.fetchall()
            if x is None:
                print "N/A"
            else:
                print "\n***********************************************************\n"
                print " ________________________________________________"
                print "| Account Number |     Amount       |    Term    |"
                print "|________________|__________________|____________|"
                for i in x:
                    cur.execute("SELECT amount,term FROM cust_fd where account_num=:1",{'1':i[0]})
                    y=cur.fetchone()
                    print "| ", repr(i[0]).rjust(12), " | ", repr(y[0]).rjust(14), " | ", repr(y[1]).rjust(8), " | "
                print "|________________|__________________|____________|"

                print "\n***********************************************************\n"
                validate=True
        raw_input("\nPress Enter To GO BACK!\n")
        admin_menu()
    ###################################################################################################################

    elif prompt==6:
        os.system('cls')
        while validate <>True:
            sum=0
            try:
                cust_id=int(raw_input("Enter Customer ID: "))
            except:
                print "Invalid ID.Please RETRY!"
                raw_input("\nPress Enter To GO BACK!\n")
                admin_menu()
            cur.execute("SELECT account_num FROM cust_account where cust_id=:1 and account_type='loan'",{'1':cust_id})
            x=cur.fetchall()
            for i in x:
                cur.execute("SELECT amount,term FROM cust_fd where account_num=:1", {'1': i[0]})
                sum+= cur.fetchone()[0]
            #print sum
            if x is None:
                print "N/A"
            else:
                cur.execute(
                    "select  cust_account.cust_id,cust_account.account_num,cust_fd.amount,cust_fd.term FROM cust_fd inner join "
                    "cust_account ON cust_fd.account_num=cust_account.account_num where cust_fd.amount >:1 and "
                    "cust_account.account_type='loan' ", {'1': sum})
                x = cur.fetchall()
                print "\n***********************************************************\n"
                print " _______________________________________________________________"
                print "| Customer ID    |     A/c Number   |    Amount    |    Term    |"
                print "|________________|__________________|______________|____________|"
                for i in x:
                    print "| ", repr(i[0]).rjust(12), " | ", repr(i[1]).rjust(14), " | ", repr(i[2]).rjust(10), " | ",repr(i[3]).rjust(8)
                print "|________________|__________________|______________|____________|"

                print "\n***********************************************************\n"
                validate=True
        raw_input("\nPress Enter To GO BACK!\n")
        admin_menu()

    ###################################################################################################################

    elif prompt==7:
        os.system('cls')
        while validate <>True:
            amount=float(raw_input("Enter Amount: "))
            if amount <=0 or amount%1000 <> 0:
                print "Enter valid Amount."
            else:
                validate=True
        cur.execute("select  cust_account.cust_id,cust_fd.amount FROM cust_fd inner join "
                    "cust_account ON cust_fd.account_num=cust_account.account_num where cust_fd.amount >:1 and "
                    "cust_account.account_type='loan' ",{'1':amount})
        x=cur.fetchall()
        print "\n***********************************************************\n"
        print " _______________________________________________________________________"
        print "| Customer ID    |     First Name   |     Last Name    |      Amount    |"
        print "|________________|__________________|__________________|________________|"
        for i in x:
            cur.execute("SELECT first_name,last_name FROM cust_info where cust_id=:1",{'1':i[0]})
            y=cur.fetchone()
            print "| ", repr(i[0]).rjust(12), " | ", repr(y[0]).rjust(14), " | ", repr(y[1]).rjust(14), " | ", repr(i[1]).rjust(12), " | "
        print "|________________|__________________|__________________|________________|"

        print "\n***********************************************************\n"
        raw_input("\nPress Enter To GO BACK!\n")
        admin_menu()

    ###################################################################################################################

    elif prompt==8:
        os.system('cls')
        loan_sum=0
        fd_sum=0
        cur.execute("SELECT cust_id,first_name,last_name from cust_info")
        x=cur.fetchall()
        for i in x:
            cur.execute("SELECT  account_num,account_type from cust_account where cust_id=:1 and account_type in('fd','loan')",{'1':i[0]})
            y=cur.fetchall()
            if len(y)==0:

                continue
            else:
                for j in y:
                    if j[1]=='fd':
                        cur.execute("SELECT amount from cust_fd where account_num=:1",{'1':j[0]})
                        fd_sum+=cur.fetchone()[0]
                    else:
                        cur.execute("SELECT amount from cust_fd where account_num=:1", {'1': j[0]})
                        loan_sum += cur.fetchone()[0]
                if loan_sum > fd_sum:
                    print "\n***********************************************************\n"
                    print " ________________________________________________________________________________________"
                    print "| Customer ID    |     First Name   |     Last Name    |      FD Sum    |      Loan Sum  |"
                    print "|________________|__________________|__________________|________________|________________|"
                    print "| ", repr(i[0]).rjust(12), " | ", repr(i[1]).rjust(14), " | ", repr(i[2]).rjust(14), " | ", repr(fd_sum).rjust(12), " | ", repr(loan_sum).rjust(12), " | "
                    print "|________________|__________________|__________________|________________|________________|"
                    print "\n***********************************************************\n"

        raw_input("\nPress Enter To GO BACK!\n")
        admin_menu()

    ###################################################################################################################

    elif prompt==9:
        os.system('cls')
        cur.execute("select i.cust_id,i.first_name,i.last_name FROM cust_info i,cust_account a where i.cust_id=a.cust_id(+) and i.cust_id not in(select cust_id from cust_account where account_type in('loan'))")
        x=cur.fetchall()
        if x is None:
            print "No Such Customer"
            raw_input("\nPress Enter To GO BACK !")
            admin_menu()
        else:
            print "\n***********************************************************\n"
            print " ______________________________________________________"
            print "| Customer ID    |     First Name   |     Last Name    |"
            print "|________________|__________________|__________________|"
            for i in x:
                print "| ", repr(i[0]).rjust(12), " | ", repr(i[1]).rjust(14), " | ", repr(i[2]).rjust(14), " | "
            print "|________________|__________________|__________________|"
            print "\n***********************************************************\n"
            raw_input("\nPress Enter To GO BACK !")
            admin_menu()

    ###################################################################################################################

    elif prompt==10:
        os.system('cls')
        cur.execute("select i.cust_id,i.first_name,i.last_name FROM cust_info i,cust_account a where i.cust_id=a.cust_id(+) and i.cust_id not in(select cust_id from cust_account where account_type in('fd'))")
        x=cur.fetchall()
        if x is None:
            print "No Such Customer"
            raw_input("\nPress Enter To GO BACK !")
            admin_menu()
        else:
            print "\n***********************************************************\n"
            print " ______________________________________________________"
            print "| Customer ID    |     First Name   |     Last Name    |"
            print "|________________|__________________|__________________|"
            for i in x:
                print "| ", repr(i[0]).rjust(12), " | ", repr(i[1]).rjust(14), " | ", repr(i[2]).rjust(14), " | "
            print "|________________|__________________|__________________|"
            print "\n***********************************************************\n"
            raw_input("\nPress Enter To GO BACK !")
            admin_menu()
    ###################################################################################################################

    elif prompt==11:
        os.system('cls')
        cur.execute("select i.cust_id,i.first_name,i.last_name FROM cust_info i,cust_account a where i.cust_id=a.cust_id(+) and i.cust_id not in(select cust_id from cust_account where account_type in('fd','loan'))")
        x=cur.fetchall()
        if x is None:
            print "No Such Customer"
            raw_input("\nPress Enter To GO BACK !")
            admin_menu()
        else:
            print "\n***********************************************************\n"
            print " ______________________________________________________"
            print "| Customer ID    |     First Name   |     Last Name    |"
            print "|________________|__________________|__________________|"
            for i in x:
                print "| ", repr(i[0]).rjust(12), " | ", repr(i[1]).rjust(14), " | ", repr(i[2]).rjust(14), " | "
            print "|________________|__________________|__________________|"
            print "\n***********************************************************\n"
            raw_input("\nPress Enter To GO BACK !")
            admin_menu()

    ###################################################################################################################
    elif prompt==0:
        os.system('cls')
        sys.exit("Thank You")

    ###################################################################################################################

    else:
        os.system('cls')
        print"Select Right Option . Try Again\n"
        raw_input("\nPress Enter To GO BACK!\n")
        admin_menu()

    ###################################################################################################################

