import cx_Oracle as cx
import transaction
con=cx.connect('bank/vishal@127.0.0.1/xe')
cur=con.cursor()
import os
def openac(cust_id):
    os.system('cls')
    prompt = int(raw_input("1. Savings\n2. Current\n3. FD\n4. GO BACK!\n"))
    amount=0
    deposit=0
    net_amount=0
    validate=False
    if prompt == 1:
        cur.execute("SELECT last_number FROM user_sequences WHERE sequence_name = 'AC'")
        account_num = cur.fetchone()[0]
        cur.execute("SELECT account_num,account_type from cust_account where cust_id=:1 and account_type='savings' and account_status !='closed'",{'1':cust_id})
        x=cur.fetchone()
        if x is not None:
            print "You Already Have a Savings Account with account number: ",x[0]
            raw_input("\nPress Enter To GO BACK !")
            openac(cust_id)
        cur.execute("INSERT INTO cust_account(account_num,cust_id,account_type) VALUES(ac.nextval,:1,'savings')",{'1':cust_id})
        cur.execute("INSERT INTO cust_bal (trans_id,account_num,account_type,amount,deposit,net_amount,"
                    "transaction_date) VALUES( trans_id.nextval,:1,'savings',:2,:3,:4,sysdate)", (account_num,amount, deposit, net_amount))
        con.commit()
        print "***Please Note You Savings Account Number: ",account_num,"***"
        raw_input("\nPress Enter To GO BACK !")
        transaction.transaction(cust_id)

    elif prompt==2:
        cur.execute("SELECT account_num,account_type from cust_account where cust_id=:1 and account_type='current'",{'1':cust_id})
        x=cur.fetchone()
        if x is not None:
            print "You Already Have a Savings Account with account number: ",x[0]
            raw_input("\nPress Enter To GO BACK !")
            openac(cust_id)
        print "Please Deposite 5000 minimum to open an current account"
        while validate <> True:
            try:
                deposit = float(raw_input("Amount to deposite:"))
            except:
                print "Enter Integer Value"
                continue
            net_amount = deposit
            amount = 0
            if deposit < 5000:
                print "Please Deposit a Minimum Sum of 5000"
            else:
                validate=True
        cur.execute("SELECT last_number FROM user_sequences WHERE sequence_name = 'AC'")
        account_num = cur.fetchone()[0]
        cur.execute("INSERT INTO cust_account(account_num,cust_id,account_type) VALUES(ac.nextval,:1,'current')",{'1':cust_id})
        cur.execute("INSERT INTO cust_bal (trans_id,account_num,account_type,amount,deposit,net_amount,"
                    "transaction_date) VALUES( trans_id.nextval,:1,'current',:2,:3,:4,sysdate)", (account_num,amount, deposit, net_amount))
        con.commit()
        print "***Please Note You Current Account Number: ", account_num, "***"
        raw_input("\nPress Enter To GO BACK !")
        transaction.transaction(cust_id)

    elif prompt == 3:
        cur.execute("SELECT last_number FROM user_sequences WHERE sequence_name = 'AC'")
        account_num = cur.fetchone()[0]
        while validate <>True:
            amount=float(raw_input("Enter Amount to be Deposited for FD:"))
            if amount <=0 or amount%1000 <> 0:
                print "Enter valid Amount."
            else:
                validate=True
        while validate <>False:
            term=int(raw_input("Enter term of Deposit(in Months): "))
            if term <12:
                print "Enter Valid Months(>12)."
            else:
                validate=False
        cur.execute("INSERT INTO cust_account(account_num,cust_id,account_type) VALUES(ac.nextval,:1,'fd')",{'1':cust_id})
        cur.execute("INSERT INTO cust_fd VALUES(:1,:2,:3)", (account_num,amount,term))
        con.commit()
        print "***Please Note You FD Account Number: ",account_num,"***"
        raw_input("\nPress Enter To GO BACK !")
        transaction.transaction(cust_id)
    elif prompt == 4:
        transaction.transaction(cust_id)
    else:
        print "You Have Choosen Wrong Option Please Retry!"
        raw_input("\nPress Enter To GO BACK !")
        openac(cust_id)