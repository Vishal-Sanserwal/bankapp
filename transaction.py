import cx_Oracle as cx
import sys
import os
import login
import openac
con=cx.connect('bank/vishal@127.0.0.1/xe')
cur=con.cursor()

def transaction(cust_id):
    os.system('cls')
    validate=False
    prompt = int(raw_input("1. Address Change\n2. Open Account\n3. Deposit money\n4. Withdraw money\n5. Transfer "
                           "Money\n6. Print Statement\n7. Account Closure\n8. Avail Loan\n9. My Accounts\n0. Logout\n"))
    ###################################################################################################################

    if prompt == 1:
        os.system('cls')
        house_no = int(raw_input("House No.: "))
        city = raw_input("City: ")
        state = raw_input("State: ")
        validate=True
        while validate != False:
            pin = int(raw_input("Zip: "))
            if len(list(str(pin))) != 6:
                print "Enter Valid 6 digit Zip"
            else:
                validate = False
        cur.execute("UPDATE cust_info set house_no=:1,city=:2,state=:3,zip=:4 where cust_id=:5",( house_no, city, state, pin,cust_id))
        con.commit()
        raw_input("\nPress Enter To GO BACK!!")
        transaction(cust_id)
    ###################################################################################################################

    elif prompt == 2:

        openac.openac(cust_id)

    ###################################################################################################################

    elif prompt == 3:
        os.system('cls')
        while validate <>True:
            try:
                account_num=int(raw_input("Enter Account Number To Deposit: "))
            except:
                print "Enter Integer Value!"
                raw_input("Press Enter To GO BACK!!")
                transaction(cust_id)
            cur.execute("SELECT account_status from cust_account where account_num=:1 and cust_id=:2",{'1':account_num,'2':cust_id})
            x=cur.fetchone()
            if x is None:
                print "Enter Valid A/c No."
                raw_input("Press Enter To GO BACK!!")
                transaction(cust_id)
            if x[0] =="closed":
                print "This Account is Closed.Please Enter an 'active' A/c No."
                raw_input("Press Enter To GO BACK!!")
                transaction(cust_id)
            dep=float(raw_input("Enter Ammount To be Deposited:    "))

            cur.execute("SELECT amount,account_type,net_amount from cust_bal where account_num=:1 ORDER BY transaction_date desc",{'1':account_num});
            x = cur.fetchone()
            if x is None:
                print "Enter Valid A/c No."
            else:
                validate=True
        bal=x[0]
        account_type=x[1]
        if bal==None:
            bal=0
        bal=x[2]
        net_amount=bal+dep
        print "Balance After Deposit: ",net_amount
        cur.execute("INSERT INTO cust_bal (trans_id,account_num,account_type,amount,deposit,net_amount,wdraw,transaction_date) "
                    "VALUES(trans_id.nextval,:1,:2,:3,:4,:5,0,sysdate)",(account_num,account_type,bal,dep,net_amount))
        con.commit()
        raw_input("\nPress Enter To GO BACK!!")
        transaction(cust_id)
    ###################################################################################################################

    elif prompt == 4:
        os.system('cls')
        while validate <>True:
            try:
                account_num = int(raw_input("Enter Account Number To Withdraw: "))
            except:
                print "Enter Integer Value!"
                raw_input("Press Enter To GO BACK!!")
                transaction(cust_id)
            cur.execute("SELECT account_status from cust_account where account_num=:1 and cust_id=:2",{'1':account_num,'2':cust_id})
            x=cur.fetchone()
            if x is None:
                print "Enter Valid A/c No."
                raw_input("Press Enter To GO BACK!!")
                transaction(cust_id)
            if x[0] =="closed":
                print "This Account is Closed.Please Enter an 'active' A/c No."
                raw_input("\nPress Enter To GO BACK!!")
                transaction(cust_id)
            widam=float(raw_input("Enter Ammount To be withdrawn:    "))
            cur.execute("SELECT amount,wdraw_limit,account_type,net_amount from cust_bal where account_num=:1 ORDER BY transaction_date desc",{'1':account_num});
            x= cur.fetchone()
            if x is None:
                print "Enter Valid A/c No."
            else:
                validate=True
        bal= x[3]
        account_type=x[2]
        wdraw_limit=x[1]
        if widam>bal :
            print "Insufficient balance in account"
            raw_input("\nPress Enter To GO BACK!!")
            transaction(cust_id)
        elif wdraw_limit>9:
            print "You Have Crossed Maximum Withdrawl Limit\n"
            raw_input("\nPress Enter To GO BACK!!")
            transaction(cust_id)
        else:
            if wdraw_limit==0:
                cur.execute("UPDATE cust_bal SET start_date=sysdate where account_num=:1", {'1': account_num});
            bal=x[3]
            net_amount=bal-widam
            print "You have withdrawn Rs.",widam," from your account"
            cur.execute("INSERT INTO cust_bal (trans_id,account_num,account_type,amount,wdraw,net_amount,deposit,"
                        "transaction_date) VALUES(trans_id.nextval,:1,:2,:3,:4,:5,0,sysdate)",(account_num, account_type, bal, widam, net_amount))

            if x[2]=='savings':
                cur.execute("UPDATE cust_bal SET wdraw_limit=:1 where account_num=:2", {'1': wdraw_limit + 1, '2': account_num});
            con.commit()
            raw_input("\nPress Enter To GO BACK!!")
            transaction(cust_id)

    ###################################################################################################################

    elif prompt == 5:
        os.system('cls')
        while validate <>True:
            try:
                account_num1 = int(raw_input("Enter Account Number To Withdraw From: "))
            except:
                print "Enter Integer Value!"
                raw_input("Press Enter To GO BACK!!")
                transaction(cust_id)
            cur.execute("SELECT account_status from cust_account where account_num=:1 and cust_id=:2",{'1':account_num1,'2':cust_id})
            x=cur.fetchone()
            if x is None:
                print "Enter Valid A/c No."
                raw_input("Press Enter To GO BACK!!")
                transaction(cust_id)
            if x[0] =="closed":
                print "This Account is Closed.Please Enter an 'active' A/c No."
                raw_input("\nPress Enter To GO BACK!!")
                transaction(cust_id)
            try:
                account_num2 = int(raw_input("Enter Account Number to Transfer To: "))
            except:
                print "Enter Integer Value!"
                raw_input("Press Enter To GO BACK!!")
                transaction(cust_id)
            cur.execute("SELECT account_status from cust_account where account_num=:1",{'1':account_num2})
            x=cur.fetchone()
            if x is None:
                print "Enter Valid A/c No."
                raw_input("Press Enter To GO BACK!!")
                transaction(cust_id)
            if x[0] =="closed":
                print "This Account is Closed.Please Enter an 'active' A/c No."
                raw_input("\nPress Enter To GO BACK!!")
                transaction(cust_id)
            cur.execute("SELECT amount,wdraw_limit,account_type,net_amount from cust_bal where account_num=:1 ORDER BY transaction_date desc",{'1':account_num1});
            x= cur.fetchone()
            cur.execute("SELECT amount,wdraw_limit,account_type,net_amount from cust_bal where account_num=:1 ORDER BY transaction_date desc",{'1':account_num2})
            y=cur.fetchone()
            if x is None:
                print "Enter Your Valid A/c No."
            elif y is None:
                print "Please Enter a Valid Transfer A/c No."
            else:
                validate=True
                widam=float(raw_input("Enter Ammount To be Transfered:    "))

        bal1= x[3]
        bal2=y[3]
        account_type=x[2]
        wdraw_limit=x[1]
        if widam>bal1 :
            print "Insufficient balance in account"
            raw_input("\nPress Enter To GO BACK!!")
            transaction(cust_id)
        elif wdraw_limit>9:
            print "You Have Crossed Maximum Withdrawl Limit\n"
            raw_input("\nPress Enter To GO BACK!!")
            transaction(cust_id)
        else:
            if wdraw_limit==0:
                cur.execute("UPDATE cust_bal SET start_date=sysdate where account_num=:1", {'1': account_num1});
            bal1=x[3]
            net_amount1=bal1-widam
            net_amount2=bal2+widam
            print "You have withdrawn Rs.",widam," from your account"
            cur.execute("INSERT INTO cust_bal (trans_id,account_num,account_type,amount,wdraw,net_amount,deposit,"
                        "transaction_date) VALUES(trans_id.nextval,:1,:2,:3,:4,:5,0,sysdate)",(account_num1, account_type, bal1, widam, net_amount1))

            cur.execute(
                "INSERT INTO cust_bal (trans_id,account_num,account_type,amount,deposit,net_amount,wdraw,transaction_date) "
                "VALUES(trans_id.nextval,:1,:2,:3,:4,:5,0,sysdate)", (account_num2, account_type, bal2, widam, net_amount2))

            if x[2]=='savings':
                cur.execute("UPDATE cust_bal SET wdraw_limit=:1 where account_num=:2", {'1': wdraw_limit + 1, '2': account_num1});
            con.commit()
            raw_input("\nPress Enter To GO BACK!!")
            transaction(cust_id)


    ###################################################################################################################


    elif prompt == 6:
        os.system('cls')
        while validate <>True:
            try:
                account_num = int(raw_input("Enter Account Number: "))
                start_date=str(raw_input("Enter Start date in format:dd/mm/yyyy: "))
                end_date = str(raw_input("Enter End date in format:dd/mm/yyyy: "))
            except:
                print "Enter Proper Value!"
                raw_input("Press Enter To GO BACK!!")
                transaction(cust_id)
            cur.execute("SELECT to_date(transaction_date,'dd/mm/yyyy'),wdraw,deposit,net_amount from cust_bal where account_num=:1 and "
                        "transaction_date between to_date(:2,'dd/mm/yyyy') and to_date(:3,'dd/mm/yyyy') ORDER BY "
                        "transaction_date desc",{'1':account_num,'2':start_date,'3':end_date});
            x= cur.fetchall()

            if len(x)==0:
                print "No Data Retrieved.Check if  A/c No is valid OR date format is As REQUIRED"
            else:
                validate=True
        print "\n***********************************************************\n"
        print " _________________________________________________________"
        print "| Transaction Date |    Debit   |   Credit   |   Amount   |"
        print "|   (yy-mm-dd)     |            |            |            |"
        print "|__________________|____________|____________|____________|"
        for i in x:
            date=str(i[0])
            print "| ",repr(date[2:11]).rjust(14)," | ",repr(i[1]).rjust(8)," | ",repr(i[2]).rjust(8)," | ",repr(i[3]).rjust(8)," |"
        print "|__________________|____________|____________|____________|"


        print "\n***********************************************************\n"
        raw_input("\nPress Enter To GO BACK!!")
        transaction(cust_id)

    ###################################################################################################################

    elif prompt==7:
        os.system('cls')
        try:
             account_num=int(raw_input("Enter A/c No. To be Closed: "))
        except:
            print "Enter Integer Value!"
            raw_input("Press Enter To GO BACK!!")
            transaction(cust_id)
        cur.execute("SELECT account_status,account_type from cust_account where account_num=:1", {'1': account_num})
        x = cur.fetchone()
        if x is None:
            print "Account Does not Exists.Please Enter a Valid A/c No."
            raw_input("\nPress Enter To GO BACK!!")
            transaction(cust_id)
        elif x[0] == "closed":
            print "This Account is Already Closed.Please Enter an 'active' A/c No."
            raw_input("\nPress Enter To GO BACK!!")
            transaction(cust_id)
        elif x[1] =='loan' or x[1] == 'fd':
            print "This Account Cannot be closed It is LOAN/FD account"
            raw_input("\nPress Enter To GO BACK!!")
            transaction(cust_id)
        cur.execute("SELECT net_amount from cust_bal where account_num=:1 ORDER BY transaction_date desc",{'1':account_num})
        x=cur.fetchone()[0]
        print "Amount :",x," will be sent to Your Address."
        cur.execute("UPDATE cust_account set account_status='closed',date_closed=sysdate where account_num=:1",{'1':account_num})
        con.commit()
        raw_input("\nPress Enter To GO BACK!!")
        transaction(cust_id)
    ###################################################################################################################

    elif prompt == 8:
        os.system('cls')
        cur.execute("SELECT account_num FROM cust_account where cust_id=:1 and account_type='loan'", {'1': cust_id})
        x=cur.fetchone()
        if x is not None:
            print "You already Have a LOAN with bank.\n***Your LOAN Account Nuber is: ",x[0],"***\n"
            raw_input("\nPress Enter To GO BACK!!")
            transaction(cust_id)

        cur.execute("SELECT account_num FROM cust_account where cust_id=:1 and account_type='savings'",{'1':cust_id})
        x=cur.fetchone()
        if x is None:
            print "\nPlease Open A Savings Account First.\n"
        else:
            cur.execute("SELECT net_amount from cust_bal where account_num=:1 ORDER BY transaction_date desc",{'1':x[0]})
            savings=cur.fetchone()[0]
            cur.execute("SELECT last_number FROM user_sequences WHERE sequence_name = 'AC'")
            account_num = cur.fetchone()[0]
            while validate <>True:
                amount=float(raw_input("Enter Amount to be Needed For LOAN:"))
                if amount <=0 or amount%1000 <> 0 or amount>(2*savings):
                    print "\nEnter valid Amount.(Please check that:\nIt is a positive number\nIt is a multiple of " \
                          "1000\nIt Does not exceed (2*Your Saving Account Balance)\nYour Saving Account Balance is:  " \
                          "",savings
                else:
                    validate=True
            while validate <>False:
                term=int(raw_input("Enter term of Deposit(in Months): "))
                if term <=0:
                    print "Enter Valid Months."
                else:
                    validate=False
            cur.execute("INSERT INTO cust_account(account_num,cust_id,account_type) VALUES(ac.nextval,:1,'loan')",{'1':cust_id})
            cur.execute("INSERT INTO cust_fd VALUES(:1,:2,:3)", (account_num,amount,term))
            con.commit()
            print "***Please Note You LOAN Account Number: ",account_num,"***"
        raw_input("\nPress Enter To GO BACK!!")
        transaction(cust_id)
    ###################################################################################################################

    elif prompt == 9:
        os.system('cls')
        cur.execute("SELECT account_num,account_type from cust_account where cust_id=:1 and account_status!='closed'",{'1':cust_id})
        x=cur.fetchall()
        print "\n***********************************************************"

        for i in x:
            print "A/c No.: "+str(i[0])+"  Type:  "+str(i[1])
        print "***********************************************************\n"
        raw_input("\nPress Enter To GO BACK!!")
        transaction(cust_id)
    ###################################################################################################################

    elif prompt == 0:
        print "Thank You"
        import start_menu

    ###################################################################################################################


    else:
        os.system('cls')
        print("You have pressed the wrong key, please try again\n")
        raw_input("\nPress Enter To GO BACK!!")
        transaction(cust_id)

    ###################################################################################################################


