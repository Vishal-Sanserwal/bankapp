import cx_Oracle as cx

con = cx.connect('bank/vishal@127.0.0.1/xe')
cur = con.cursor()


def create_tables():
    try:
        cur.execute("CREATE SEQUENCE id START WITH 1000 INCREMENT BY 1 NOCACHE NOCYCLE")
    except cx.DatabaseError as e:
        pass
    try:
        cur.execute("CREATE SEQUENCE trans_id START WITH 1000 INCREMENT BY 1 NOCACHE NOCYCLE")
    except cx.DatabaseError as e:
        pass
    try:
        cur.execute("CREATE SEQUENCE ac START WITH 1786 INCREMENT BY 1 NOCACHE NOCYCLE")
    except cx.DatabaseError as e:
        pass
    try:
        cur.execute("CREATE TABLE  cust_info ( cust_id int PRIMARY KEY, first_name varchar(50) NOT NULL, last_name "
                    "varchar(50) NOT NULL, passwd varchar(50) NOT NULL,attempts int default 0,house_no int NOT NULL, "
                    "city varchar(50) NOT NULL,state varchar(20) NOT NULL, zip numeric(6) NOT NULL)")
    except:
        #print e
        pass
    try:
        cur.execute("CREATE TABLE  cust_account (account_num int PRIMARY KEY,cust_id int NOT NULL,account_type varchar(8),"
                    "account_status varchar(10) default 'active',date_closed date, CONSTRAINT fk_cust FOREIGN KEY ("
                    "cust_id) REFERENCES cust_info(cust_id))")
    except:
        #print e
        pass
    try:
        cur.execute("CREATE TABLE cust_bal(account_num int NOT NULL,trans_id int PRIMARY KEY,account_type varchar(8) NOT NULL,amount float(8) "
                    "DEFAULT 0.0,wdraw float(8) DEFAULT 0.0,deposit float(8) DEFAULT 0.0,net_amount float(8) DEFAULT "
                    "0.0,wdraw_limit int default 0,start_date date,transaction_date date,constraint fkey1 foreign "
                    "key(account_num) references cust_account(account_num))")
    except cx.DatabaseError as e:
        #print e
        pass
    try:
        cur.execute("CREATE TABLE cust_fd(account_num int NOT NULL,amount float(8) NOT NULL,term int NOT NULL,"
                    "CONSTRAINT fkey2 FOREIGN KEY(account_num) REFERENCES cust_account(account_num))")
    except cx.DatabaseError as e:
        #print e
        pass
    finally:
        pass
