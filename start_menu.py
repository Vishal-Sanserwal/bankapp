import sys
import register
import login
import admin_menu
# This is the function that is called at the beginning of the program
import os
import getpass
def bankMenu():
    os.system('cls')
    try:
        prompt = int(raw_input("""1. Sign Up (New Customer)\n""" +
                               """2. Sign In (Existing Customer)\n""" +
                               """3. Admin Sign In\n""" +
                               """4. Quit\n"""))
    except:
        os.system('cls')
        print "Enter Valid Input"
        raw_input("\nPress Enter to GO Back!")
        bankMenu()
    if prompt == 1:
        # pass creates a new customer profile
        x = register.register()
        if x == 2:

            bankMenu()
        bankMenu()
    elif prompt == 2:
        x = login.login()
        if x == 0:
            bankMenu()
    elif prompt == 3:
        os.system('cls')
        try:
            username=raw_input("Username: ")
            password=getpass.getpass("Password: ")
        except:
            print "Enter Valid Credentials"
            bankMenu()
        if username=='bank' and password=='vishal':
            admin_menu.admin_menu()
        else:
            print "Invalid Credentials.Please Retry!!"
            raw_input("Press Enter")
            bankMenu()
    elif prompt == 4:
        sys.exit("Thank You")
    else:
        print "You have pressed the wrong key, please try again\n"
        bankMenu()


bankMenu()
