import cx_Oracle as cx
import sys
con = cx.connect('bank/vishal@127.0.0.1/xe')
cur = con.cursor()
import os
import getpass
from base64 import b64encode
def login():
    os.system('cls')
    try:
        cust_id = int(raw_input("User Id: "))
        passwd = getpass.getpass("Password: ")
    except:
        print "Enter Valid Credentials"
        raw_input("\nPress Enter!")
        login()
    passwd=b64encode(passwd)
    cur.execute("SELECT * from cust_info where cust_id=:1 and passwd=:2",(cust_id,passwd))
    x=cur.fetchone()
    if x is not None:
        if x[6]=="closed":
            print "This Account has been closed.Please Contact Branch Administartor"
            sys.exit()
    try:
        cur.execute("SELECT attempts from cust_info where cust_id=:1",{'1':cust_id})
        y=cur.fetchone()
        attempts=y[0]
    except:
        print "No such User Exists.Please Enter a Valid User Id"
        raw_input("\nPress Enter!")
        login()
    if x is None:
        print "Login Failure Please Retry "
        if attempts<2 and attempts>=0:
            attempts+=1
            print "\n\nAttempts Remaining",3-attempts
            cur.execute("UPDATE cust_info SET attempts=:1 WHERE cust_id=:2",{'1':attempts,'2':cust_id})
            con.commit()
            raw_input("\nPress Enter!")
            login()
        else:
            print "\nAccount Locked Please contact Administrator!!\n"
            return 0;
    else:
        #print x[5]
        if x[5]==2:
            print "\nAccount Locked Please Contact Branch Administrator!!\n"
            return 0;
        print "\nLogin Successful!\n"
        raw_input("\nPress Enter For Menu!!!\n")
        cur.execute("UPDATE cust_info SET attempts=:1 WHERE cust_id=:2", {'1': 0, '2': cust_id})
        con.commit()
        import transaction
        transaction.transaction(cust_id)

    try:
        con.close()
    except:
        pass
